# Rest Demo with Spring Boot / Jersey and Spring Data #

This Java Application also creates a Rest API for Users/Tasks. This app uses SpringBoot/Jersey and SpringData with HSQLDB.
The rest endpoints have integration tests written using Spring's Rest Template Client.

## REST URLS FOR READ/GET ##

* http://localhost:8080/users
* http://localhost:8080/users/{userid}
* http://localhost:8080/users/{userid}/tasks
* http://localhost:8080/users/{userid}/tasks/{taskid}
* http://localhost:8080/tasks
* http://localhost:8080/tasks/{taskid}
* http://localhost:8080/tasks/{taskid}/user (should redirect back to http://localhost:8080/users/{userId} )

## REST URLS POST/CREATE ##

to create a new user 

post to http://localhost:8080/users

```
#!python

curl -i -X POST -H "Content-Type: application/json" -d '{"firstname" : "Saint","lastname" : "Nicholas", "username" : "santa"}' http://localhost:8080/users

HTTP/1.1 201 Created

Location: http://localhost:8080/users/4
```

to create a new user task

post to http://localhost:8080/users/1/tasks

```
#!python

curl -i -X POST -H "Content-Type: application/json" -d '{"dueDate":"2017-04-01T01:02:03Z" , "priority":"MED" , "completed":false, "description": "a new task"}' http://localhost:8080/users/1/tasks

HTTP/1.1 201 Created

Location: http://localhost:8080/tasks/24
```

To run the java app


```
#!python

$ mvn spring-boot:run
```