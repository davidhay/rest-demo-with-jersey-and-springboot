insert into user(username,first,last,creation_ts,update_ts,version,id) values ('joebloggs','Joe','Bloggs',now(),null,0,1)
insert into user(username,first,last,creation_ts,update_ts,version,id) values ('molliem','Mollie','Malone',now(),null,0,2)

insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task eleven'       ,1   ,'LO'    ,'2016-01-01 01:02:03'  ,11,0      ,now()      ,null     ,'N')
insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task twelve'       ,1   ,'MED'   ,'2016-01-02 02:03:04'  ,12,0      ,now()      ,null     ,'Y')
insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task thirteen'     ,1   ,'HI'    ,'2016-01-03 03:04:05'  ,13,0      ,now()      ,null     ,'N')

insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task twenty one'   ,2   ,'LO'    ,'2016-01-04 04:05:06'  ,21, 0      ,now()      ,null     ,'N')
insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task twenty two'   ,2   ,'MED'   ,'2016-01-05 05:06:07'  ,22, 0      ,now()      ,null     ,'Y')
insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task twenty three' ,2   ,'HI'    ,'2016-01-06 06:07:08'  ,23, 0      ,now()      ,null     ,'N')

