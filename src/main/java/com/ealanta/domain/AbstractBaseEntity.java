package com.ealanta.domain;

/**
 * @author davidhay
 */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.GenerationTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

@MappedSuperclass
public abstract class AbstractBaseEntity implements Serializable {

    private static final long serialVersionUID = 5674049871176760230L;

	@Id
	@GeneratedValue
	private Long id;

	@JsonIgnore
    @Version
    @Column(name = "version")
    private Long version;

	@JsonIgnore
    @Column(name = "creation_ts")
    @Temporal(TemporalType.TIMESTAMP)
    @org.hibernate.annotations.Generated(value = GenerationTime.INSERT)
    private Date creationTimestamp;

	@JsonIgnore
    @Column(name = "update_ts")
    @Temporal(TemporalType.TIMESTAMP)
    @org.hibernate.annotations.Generated(value = GenerationTime.ALWAYS)
    private Date updateTimestamp;

    public AbstractBaseEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getCreationTimestamp() {
        return this.creationTimestamp;
    }

    public Date getUpdateTimestamp() {
        return this.updateTimestamp;
    }

}
