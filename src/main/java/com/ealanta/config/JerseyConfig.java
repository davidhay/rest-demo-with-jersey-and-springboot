package com.ealanta.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.ealanta.endpoints.HealthEndpoint;
import com.ealanta.endpoints.TaskEndpoint;
import com.ealanta.endpoints.UserEndpoint;

@Component
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(UserEndpoint.class);
		register(TaskEndpoint.class);
		register(HealthEndpoint.class);
	}

}

