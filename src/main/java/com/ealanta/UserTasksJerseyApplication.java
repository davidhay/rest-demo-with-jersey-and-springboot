package com.ealanta;

import java.text.SimpleDateFormat;

import javax.servlet.Filter;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;

@SpringBootApplication
public class UserTasksJerseyApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(UserTasksJerseyApplication.class);
	}

	public static void main(String[] args) {
		new UserTasksJerseyApplication().configure(
				new SpringApplicationBuilder(UserTasksJerseyApplication.class)).run(args);
	}
	

	@Configuration
	static class Config {
		
		@Bean
		Filter openEntityManagerInViewFilter(){
			OpenEntityManagerInViewFilter filter = new OpenEntityManagerInViewFilter();
			return filter;
		}
		
	}
}
