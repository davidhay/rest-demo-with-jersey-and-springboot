package com.ealanta.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ealanta.domain.Task;

/**
 * @author davidhay
 */
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {

	List<Task> findByUserUsernameIgnoringCase(String username);

}
