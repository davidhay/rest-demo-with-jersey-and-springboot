package com.ealanta.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ealanta.domain.User;

/**
 * @author davidhay
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

	User findByUsernameAllIgnoringCase(String username);

}
