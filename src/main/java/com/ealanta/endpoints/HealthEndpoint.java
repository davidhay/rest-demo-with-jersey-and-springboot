package com.ealanta.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.stereotype.Component;

import com.ealanta.domain.Health;

@Component
@Path("/health")
public class HealthEndpoint {
	
	@GET
	@Produces("application/json")
	public Health health() {
		return new Health("Jersey: Up and Running!");
	}
	
}
