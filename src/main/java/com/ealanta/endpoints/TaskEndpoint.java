package com.ealanta.endpoints;

import java.net.URI;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.stereotype.Component;

import com.ealanta.domain.Task;
import com.ealanta.domain.User;
import com.ealanta.repository.TaskRepository;

@Component
@Path("/tasks")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class TaskEndpoint {

	@Inject
	private TaskRepository taskRepository;

	@GET
	public Iterable<Task> getTasks() {
		Iterable<Task> result = taskRepository.findAll();
		return result;
	}

	@GET
	@Path("{id}")
	public Task findOne(@PathParam("id") Long id) {
		return taskRepository.findOne(id);
	}

	@GET
	@Path("{id}/user")
	public Response getUserTasks(@PathParam("id") Long id, @Context UriInfo uriInfo) {
		Task task = taskRepository.findOne(id);
		User user = task.getUser();
		Long userId = user.getId();
		URI location = uriInfo.getBaseUriBuilder().path("users/{id}").resolveTemplate("id", userId).build();
		return Response.seeOther(location).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") Long id) {
		taskRepository.delete(id);
		return Response.accepted().build();
	}

}
