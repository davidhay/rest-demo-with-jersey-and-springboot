package com.ealanta.endpoints;

import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.stereotype.Component;

import com.ealanta.domain.Task;
import com.ealanta.domain.User;
import com.ealanta.repository.TaskRepository;
import com.ealanta.repository.UserRepository;

@Component
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class UserEndpoint {

	@Inject
	private UserRepository userRepository;

	@Inject
	private TaskRepository taskRepository;

	@SuppressWarnings("unchecked")
	@GET
	public List<User> getUsers() {
		Iterator<User> iter = userRepository.findAll().iterator();
		return IteratorUtils.toList(iter);
	}

	@GET
	@Path("{id}")
	public User findOne(@PathParam("id") Long id) {
		return userRepository.findOne(id);
	}

	@GET
	@Path("{id}/tasks")
	public List<Task> getUserTasks(@PathParam("id") Long id) {
		User user = userRepository.findOne(id);
		List<Task> tasks = user.getTasks();
		return tasks;
	}

	@GET
	@Path("{userId}/tasks/{taskId}")
	public Response getUserTasks(@PathParam("userId") Long userId,
			@PathParam("taskId") Long taskId) {
		User user = userRepository.findOne(userId);
		Optional<Task> task = user.getTasks().stream()
				.filter(t -> t.getId() == taskId).findFirst();
		if (task.isPresent()) {
			return Response.ok(task.get()).build();
		} else {
			return Response.status(Status.NOT_FOUND)
					.entity("cannot find task " + taskId).build();
		}
	}

	@POST
	public Response save(User user, @Context UriInfo uriInfo) {
		try {
			User saved = userRepository.save(user);
			System.out.println(saved);
			System.out.printf("id is %s%n",saved.getId());
			System.out.flush();

			UriBuilder builder1 = uriInfo.getAbsolutePathBuilder();
			UriBuilder builder2 = builder1.path("{id}");
			UriBuilder builder3 = builder2.resolveTemplate("id", saved.getId());
			URI location = builder3.build();

			return Response.created(location).build();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@POST
	@Path("{id}/tasks")
	public Response saveUserTask(Task task, @Context UriInfo uriInfo, @PathParam("id") Long userId) {
		try {
			User user = userRepository.findOne(userId);
			task.setUser(user);
			Task saved = taskRepository.save(task);
			
			System.out.printf("id is %s%n",saved.getId());
			System.out.flush();

			UriBuilder builder1 = uriInfo.getBaseUriBuilder();
			UriBuilder builder2 = builder1.path("/tasks/{id}");
			UriBuilder builder3 = builder2.resolveTemplate("id", saved.getId());
			URI location = builder3.build();

			return Response.created(location).build();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") Long id) {
		userRepository.delete(id);
		return Response.accepted().build();
	}
}
