package com.ealanta;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.ealanta.UserTasksJerseyApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserTasksJerseyApplication.class)
@WebAppConfiguration
public class UserTasksJerseyApplicationTests {

	@Test
	public void contextLoads() {
	}

}
