package com.ealanta.endpoints;

import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.theories.DataPoint;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.ealanta.UserTasksJerseyApplication;
import com.ealanta.domain.Priority;
import com.ealanta.domain.Task;
import com.ealanta.domain.User;
import com.jayway.jsonpath.JsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserTasksJerseyApplication.class)
@IntegrationTest("server.port=0")
@WebAppConfiguration
public class UserEndpointTest {

	@Value("${local.server.port}")
	private int port;

	private RestTemplate restTemplate = new TestRestTemplate();


	private String getUrl(String urlPath) {
		String temp = urlPath;
		while (temp.startsWith("/")) {
			temp = temp.substring(1);
		}
		return "http://localhost:" + this.port + "/" + temp;
	}

	private void checkJoe(User user) {
		Assert.assertEquals(1L, user.getId().longValue());
		Assert.assertEquals("Joe", user.getFirstname());
		Assert.assertEquals("Bloggs", user.getLastname());
		Assert.assertEquals("joebloggs", user.getUsername());
	}

	private void checkMollie(User user) {
		Assert.assertEquals(2L, user.getId().longValue());
		Assert.assertEquals("Mollie", user.getFirstname());
		Assert.assertEquals("Malone", user.getLastname());
		Assert.assertEquals("molliem", user.getUsername());
	}

	@Test
	public void testGetUsers() {
		User[] users = this.restTemplate.getForObject(getUrl("/users"),
				User[].class);
		Assert.assertEquals(2, users.length);
		User user1 = users[0];
		User user2 = users[1];

		checkJoe(user1);
		checkMollie(user2);
	}
	
	@Test
	public void testGetUserJoe() {
		User user = this.restTemplate.getForObject(getUrl("/users/1"),
				User.class);
		checkJoe(user);
	}

	@Test
	public void testGetEntityUserJoe() {
		ResponseEntity<User> result = this.restTemplate.getForEntity(getUrl("/users/1"),
				User.class);
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		checkJoe(result.getBody());
	}
	
	@Test
	public void testGetRawUserJoe() {
		ResponseEntity<String> result = this.restTemplate.getForEntity(getUrl("/users/1"),
				String.class);
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertEquals(new Integer(1),JsonPath.read(result.getBody(),"$.id"));
		Assert.assertEquals("joebloggs",JsonPath.read(result.getBody(),"$.username"));
		Assert.assertEquals("Joe",JsonPath.read(result.getBody(),"$.firstname"));
		Assert.assertEquals("Bloggs",JsonPath.read(result.getBody(),"$.lastname"));
		Assert.assertEquals("2016-01-01T01:02:03Z",JsonPath.read(result.getBody(),"$.tasks[0].dueDate"));
		Assert.assertEquals("2016-01-02T02:03:04Z",JsonPath.read(result.getBody(),"$.tasks[1].dueDate"));
		Assert.assertEquals("2016-01-03T03:04:05Z",JsonPath.read(result.getBody(),"$.tasks[2].dueDate"));
	}
	
	@Test
	public void testGetRawUserJoeTasks() {
		ResponseEntity<Task[]> result = this.restTemplate.getForEntity(getUrl("/users/1/tasks"),
				Task[].class);
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertEquals(3,result.getBody().length);
	}
	
	@Test
	public void testGetRawUserJoeTask11() {
		ResponseEntity<Task> result = this.restTemplate.getForEntity(getUrl("/users/1/tasks/11"),
				Task.class);
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Task task = result.getBody();
		Assert.assertEquals(false,task.getCompleted());
		Assert.assertEquals("task eleven",task.getDescription());
		Assert.assertEquals(11L,task.getId().longValue());
		Assert.assertEquals(Priority.LO,task.getPriority());
		
		checkDate(task.getDueDate(),2016,1,1 ,1,2,3);
	}
	
	@Test
	public void testGetRawUserJoeTask12() {
		ResponseEntity<Task> result = this.restTemplate.getForEntity(getUrl("/users/1/tasks/12"),
				Task.class);
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Task task = result.getBody();
		Assert.assertEquals(true,task.getCompleted());
		Assert.assertEquals("task twelve",task.getDescription());
		Assert.assertEquals(12L,task.getId().longValue());
		Assert.assertEquals(Priority.MED,task.getPriority());
		
		checkDate(task.getDueDate(),2016,1,2 ,2,3,4);
	}
	private void checkDate(Date date, int year, int month, int day, int hour, int min, int sec){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Assert.assertEquals(year, cal.get(Calendar.YEAR));
		Assert.assertEquals(month-1, cal.get(Calendar.MONTH));
		Assert.assertEquals(day, cal.get(Calendar.DAY_OF_MONTH));
		Assert.assertEquals(hour, cal.get(Calendar.HOUR_OF_DAY));
		Assert.assertEquals(min, cal.get(Calendar.MINUTE));
		Assert.assertEquals(sec, cal.get(Calendar.SECOND));
	}
	
	
	@Test
	public void testGetUserMollie() {
		User user = this.restTemplate.getForObject(getUrl("/users/2"),
				User.class);
		checkMollie(user);
	}
	
	@Test
	@DirtiesContext
	public void testSaveNewUser() {
		User newUser = new User();
		newUser.setFirstname("new-first");
		newUser.setLastname("new-last");
		newUser.setUsername("newuser");
		URI location = this.restTemplate.postForLocation(getUrl("/users"),newUser);
		Assert.assertNotNull(location);
		String regex = "(http://localhost:[0-9]+/users/)([0-9]+)";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(location.toString());
		Assert.assertTrue(m.find());
		String newUserId = m.group(2);
		Assert.assertEquals("3",newUserId);
		System.out.println(location.toString());
	}
	
	private Date getDate(int dayOfMonth, int month, int year){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 12);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.DAY_OF_MONTH,dayOfMonth);
		cal.set(Calendar.MONTH,month-1);
		cal.set(Calendar.YEAR,year);
		return cal.getTime();
	}
	
	@Test
	@DirtiesContext
	public void testSaveNewUserTask() {
		Task newTask = new Task();
		newTask.setCompleted(false);
		newTask.setDescription("a new task");
		newTask.setDueDate(getDate(1,4,2015));
		newTask.setPriority(Priority.MED);
		
		URI location = this.restTemplate.postForLocation(getUrl("/users/1/tasks"),newTask);
		Assert.assertNotNull(location);
		String regex = "(http://localhost:[0-9]+/tasks/)([0-9]+)";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(location.toString());
		Assert.assertTrue(m.find());
		String newUserId = m.group(2);
		Assert.assertEquals("24",newUserId);
		System.out.println(location.toString());
		
		ResponseEntity<Task> response = restTemplate.getForEntity(location, Task.class);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Task task = response.getBody();
		Assert.assertEquals(false,task.getCompleted());
		Assert.assertEquals(Priority.MED,task.getPriority());
		Assert.assertEquals("a new task",task.getDescription());
		//DUE DATE
		Calendar  cal = Calendar.getInstance();
		cal.setTime(task.getDueDate());
		Assert.assertEquals(1, cal.get(Calendar.DAY_OF_MONTH));
		Assert.assertEquals(Calendar.APRIL, cal.get(Calendar.MONTH));
		Assert.assertEquals(2015, cal.get(Calendar.YEAR));
		
	}

}
